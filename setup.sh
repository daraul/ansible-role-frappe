#!/usr/bin/env bash

export ANSIBLE_ROLES_PATH=.roles
export ANSIBLE_COLLECTIONS_PATH=.collections
export PATH=$PATH:`pwd`/bin
make venv/bin/activate
source venv/bin/activate
