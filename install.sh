#!/usr/bin/env bash

set -euo pipefail

source setup.sh
pip install -r requirements.txt
ansible-galaxy install -r requirements.yml
